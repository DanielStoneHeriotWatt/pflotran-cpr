module CPR_linsolver
!! Implements a CPR preconditioner using the PCSHELL
!! funcitonality of PETSC.
!! Daniel Stone and Sebastien Loisel
#include <petsc/finclude/petscsys.h>
#include "petsc/finclude/petscts.h"
#include "petsc/finclude/petscmat.h"
#include "petsc/finclude/petscvec.h"
#include "petsc/finclude/petscpc.h"
#include "petsc/finclude/petscviewer.h"
    use petscmat
    use petscpc
    use petscvec
    use quickUtils
    use PFLOTRAN_Constants_module
    implicit none
    type CPRPC
        Mat :: A, Ap
        !! The two stage CPR preconditioner calls two other preconditioners:
        PC :: T1        !! T1 will be a SHELL pc that extracts the pressure system residual from the
                        !! input residual, then approximates the inverse of Ap acting on that residual
                        !! using AMG.
        KSP :: T1solver !! Usually PCONLY, the PC for this KSP is hypre/boomeramg. This is called
                        !! by T1 (above) as the AMG part.
        PC :: T2        !! A regular PC, usually BJACOBI
        Vec :: T1r,r2, s, z, factors1vec, factors2vec
        PetscInt ::  t2fillin, timescalled, asmoverlap, exrow_offset
        PetscBool :: firstT1Call, firstT2call, asmfactorinplace, &
                     t2shiftinblocks, zeroing, useGAMG, mv_output, t2_zeroing, &
                     skip_T1, amg_report, amg_manual
        character(len=MAXWORDLENGTH) :: T1_type, T2_type, extract_type
        !! following are buffers/workers for the pressure system extraction.
        !! 1d arrays:
        PetscReal, dimension(:), allocatable :: vals, insert_vals
        PetscInt, dimension(:), allocatable :: colIdx, colIdx_keep, insert_colIdx
        !! 2d arrays:
        PetscReal, dimension(:,:), allocatable :: all_vals
    end type CPRPC

    INTERFACE
      SUBROUTINE PCShellSetContext (P_in, ctx_in, ierr_in)
        use petscksp
        Import :: CPRPC
        PC :: P_in
        type(CPRPC) :: ctx_in
        PetscErrorCode :: ierr_in
      END SUBROUTINE PCShellSetContext
    END INTERFACE

    INTERFACE
      SUBROUTINE PCShellGetContext (P_in, ctx_in, ierr_in)
        use petscksp
        Import :: CPRPC
        PC :: P_in
        type(CPRPC), pointer :: ctx_in
        PetscErrorCode :: ierr_in
      END SUBROUTINE PCShellGetContext
    END INTERFACE

contains

!*******************************************************
!  CPR Apply routines

  subroutine CPRApply(P, r, y,ierr)
    implicit none
      PC :: P
      Vec :: r
      Vec :: y
      Vec :: T1r, r2
      PetscErrorCode :: ierr
      PetscReal one
      type(CPRPC), pointer :: ctx
      PetscInt Ap,Aq,AM,AN,b;
      MPI_Comm C
      Mat A
      real :: fin, st
      PetscReal :: vnm, mf, mnf
      PetscInt :: mdx, mndx

      call PCShellGetContext(P, ctx, ierr); CHKERRQ(ierr)

      one = 1.0

      r2 = ctx%r2
      T1r = ctx%T1r
      A = ctx%A

      if (ctx%skip_T1) then
        call VecZeroEntries(T1r,ierr); CHKERRQ(ierr)
      else
        call PCApply(ctx%T1,r,T1r,ierr); CHKERRQ(ierr)
      endif

      call MatMult(A,T1r,r2,ierr); CHKERRQ(ierr)
      call VecAYPX(r2,-one,r,ierr); CHKERRQ(ierr) !! r1 <- r-r2

      call PCApply(ctx%T2,r2,y,ierr); CHKERRQ(ierr)

      call quickVecBinOut(r2, "r2", ierr, ctx%mv_output)

      if (ctx%t2_zeroing) then
        call VecStrideSet(y, 0, 0.d0, ierr); CHKERRQ(ierr)
      endif

      !! this is for debugging and will not do anything unless foo%mv_output
      !! is true
      call quickVecBinOut(y, "y_before_add", ierr, ctx%mv_output)
      !! /debugging output stuff

      call VecAYPX(y,one,T1r,ierr); CHKERRQ(ierr)

      !! this is for debugging and will not do anything unless foo%mv_output
      !! is true
      call quickVecBinOut(y, "y_after_add", ierr, ctx%mv_output)
      call quickVecBinOut(T1r, "T1r", ierr, ctx%mv_output)
      !! /debugging output stuff

  end subroutine CPRApply

  subroutine CPRT1Apply(P, x, y,ierr)
      PC :: P
      Vec :: x
      Vec :: y
      PetscErrorCode :: ierr
      type(CPRPC), pointer :: ctx
      KSP solver
      PC amgsolver
      !! start and end used?
      PetscInt b,start,end,k,its
      !Vec,save :: s,z
      !logical,save :: firstCall = .TRUE.
      Vec :: s, z
      PetscInt AM,AN,Aq,Ar
      MPI_Comm C
      Mat :: Ahere, A2here

      real :: st, fin

      call PCShellGetContext(P, ctx, ierr); CHKERRQ(ierr)
      s = ctx%s
      z = ctx%z

      if (ctx%extract_type .EQ. "FIMPES") then
        call MatGetOwnershipRange(ctx%A, start, end, ierr); CHKERRQ(ierr)
        k = 0
        call VecStrideGather(x,k,s,INSERT_VALUES,ierr); CHKERRQ(ierr)
        call MatGetBlockSize(ctx%A,b,ierr); CHKERRQ(ierr)
        do k=1,b-1
            call VecStrideGather(x,k,s,ADD_VALUES,ierr); CHKERRQ(ierr)
        end do
      else
        call QIRHS(ctx%factors1Vec, ctx%factors2vec, x, s, ierr)
      endif

      solver = ctx%T1solver

      !! this is for debugging and will not do anything unless foo%mv_output
      !! is true
      call quickMatBinOut(ctx%Ap, "Apmat", ierr, ctx%mv_output)
      call quickMatBinOut(ctx%A, "Amat", ierr, ctx%mv_output)
      call quickVecBinOut(s, "pres_sys_rhs", ierr, ctx%mv_output)
      call quickVecBinOut(x, "whole_sys_rhs", ierr, ctx%mv_output)
      call quickVecBinOut(ctx%factors1Vec, "factors1", ierr, ctx%mv_output)
      call quickVecBinOut(ctx%factors2Vec, "factorsworker", ierr, ctx%mv_output)
      !! /debugging output stuff

      if (ctx%T1_type .NE. "NONE") then
        call KSPSolve(solver,s,z,ierr); CHKERRQ(ierr)
        call KSPGetIterationNumber(solver, its, ierr); CHKERRQ(ierr)
      else
        call KSPGetPC(solver, amgsolver, ierr);CHKERRQ(ierr)
        call PCApply(amgsolver,s,z,ierr); CHKERRQ(ierr)

        if (ctx%amg_report) then
          call PCView(amgsolver, PETSC_VIEWER_STDOUT_SELF, ierr);CHKERRQ(ierr)
        endif
      endif

      !! this is for debugging and will not do anything unless foo%mv_output
      !! is true
      call quickVecBinOut(z, "pres_sys_sol", ierr, ctx%mv_output)
      !! /debugging output stuff

      call VecZeroEntries(y,ierr); CHKERRQ(ierr)
      call VecStrideScatter(z,0,y,INSERT_VALUES,ierr); CHKERRQ(ierr)

  end subroutine CPRT1Apply

!  end of CPR apply routines
!*******************************************************

!*******************************************************
!  CPR Setup Routines (every time Jaocobian updates) 

  subroutine CPRSetup(P,ierr)
      PC :: P
      PetscErrorCode :: ierr
      type(CPRPC), pointer ::ctx

      call PCShellGetContext(P, ctx, ierr); CHKERRQ(ierr)
      call CPRSetupT1(ctx, ierr)
      call CPRSetupT2(ctx, ierr)

  end subroutine CPRSetup

  subroutine CPRSetupT1(ctx,  ierr)
      PetscErrorCode :: ierr
      PetscInt b,Aq,Ar,AM,AN, mx
      Mat A
      KSP solver
      PC prec
      type(CPRPC) :: ctx
      real :: fin, st
      PetscBool :: isassembled

      A = ctx%A

      call MatGetBlockSize(A,b,ierr); CHKERRQ(ierr)

      if (ctx%firstT1Call) then !! some final initializations that need knowlege
                                !! of the fully assembled Jacobian
        !! let T1 and it's inner solver know what their inner operators are
        solver = ctx%T1solver
        call PCSetOperators(ctx%T1,A,A,ierr); CHKERRQ(ierr)
        call KSPSetOperators(solver,ctx%Ap,ctx%Ap,ierr); CHKERRQ(ierr)


        !! now sparsity pattern of the Jacobian is defined, 
        !! allocate worker arrays that are big enough to 
        !! be used in the extraction routines coming up.
        call MatGetMaxRowCount(A, mx, ierr)
        call AllocateWorkersInCPRStash(ctx, mx, b)

        ctx%firstT1Call = .FALSE.
      end if

      call MatZeroEntries(ctx%Ap, ierr); CHKERRQ(ierr)

      select case(ctx%extract_type)
        case('QIMPES_VARIABLE')
           if (b .EQ. 3) then
            !! we have a more efficient version for 3x3 blocks so do that if we can instead
            call MatGetSubQIMPES(A, ctx%Ap, ctx%factors1vec, ctx%factors2vec, ierr, ctx)
          else
            call MatGetSubQIMPES_var(A, ctx%Ap, ctx%factors1vec, ctx%factors2vec, ierr, AN, ctx%zeroing, b, ctx%mv_output, ctx)
          endif
        case('QIMPES_VARIABLE_FORCE')
          !! force to use the variables block size implementation even for 3x3 blocks
          call MatGetSubQIMPES_var(A, ctx%Ap, ctx%factors1vec, ctx%factors2vec, ierr, AN, ctx%zeroing, b, ctx%mv_output, ctx)
        case('FIMPES')
          !! FIMPES extraction - I don't think we'll ever really want this again but who knows
          call MatGetSubFIMPES(A, ctx%Ap, ierr)
        case default
      end select

  end subroutine CPRSetupT1

  subroutine CPRSetupT2(ctx, ierr)
      PetscErrorCode :: ierr
      MPI_Comm C
      PC T2,P
      Mat A
      KSP solver
      type(CPRPC) :: ctx

      PetscInt :: nsub_ksp, first_sub_ksp, i
      KSP, pointer :: sub_ksps(:)
      PC :: pc_inner

      !! set operator on first call.

      !! also if first call then time is right
      !! to adjust things like fill in, etc. 


      !! if BJACOBI, this is the best place to try
      !! modifying sub-ksps
      if (ctx%firstT2Call) then

      T2 = ctx%T2

        !if (ctx%T2_type .EQ. PCASM) then
        if (trim(ctx%T2_type) .EQ. 'PCASM') then
          call PCASMSetOverlap(T2, ctx%asmoverlap, ierr);CHKERRQ(ierr)
        endif

        A = ctx%A
        call PCSetOperators(T2,A,A,ierr); CHKERRQ(ierr)
        call PCSetFromOptions(T2, ierr);CHKERRQ(ierr)
        call PCSetUp(T2, ierr); CHKERRQ(ierr)

        !if (ctx%T2_type .EQ. PCASM) then
        if (trim(ctx%T2_type) .EQ. 'PCASM') then

          !! default should be preonly
          call PCASMGetSubKSP(T2,   nsub_ksp, first_sub_ksp, &
                                   PETSC_NULL_KSP, ierr); CHKERRQ(ierr)
          !                         ksp array
          ! allocate ksp array now number known:
          allocate(sub_ksps(nsub_ksp))
          ! call again:
          call PCASMGetSubKSP(T2,   nsub_ksp, first_sub_ksp, &
                                   sub_ksps, ierr); CHKERRQ(ierr)

          do i = 1,nsub_ksp
            call KSPGetPC(sub_ksps(i), pc_inner);CHKERRQ(ierr)
            if (ctx%t2shiftinblocks) then
              call PCFactorSetShiftType(pc_inner,MAT_SHIFT_INBLOCKS,ierr);CHKERRQ(ierr)
            endif
            call PCFactorSetLevels(pc_inner, ctx%t2fillin, ierr); CHKERRQ(ierr)
            if (ctx%asmfactorinplace) then
              call PCFactorSetUseInPlace(pc_inner, PETSC_TRUE,  ierr);CHKERRQ(ierr)
            endif
          enddo
          deallocate(sub_ksps)
          nullify(sub_ksps)
        !! specifically for block jacobi:
        !elseif (ctx%T2_type .EQ. PCBJACOBI) then
        elseif (trim(ctx%T2_type) .EQ. 'PCBJACOBI') then

          !! default should be preonly
          call PCBJacobiGetSubKSP(T2,   nsub_ksp, first_sub_ksp, &
                                   PETSC_NULL_KSP, ierr); CHKERRQ(ierr)
          !                         ksp array
          ! allocate ksp array now number known:
          allocate(sub_ksps(nsub_ksp))
          ! call again:
          call PCBJacobiGetSubKSP(T2,   nsub_ksp, first_sub_ksp, &
                                   sub_ksps, ierr); CHKERRQ(ierr)

          do i = 1,nsub_ksp
            !! default should be ilu with 0 fill
            call KSPGetPC(sub_ksps(i), pc_inner, ierr); CHKERRQ(ierr)

            !! MUST DO THIS: the pc is otherwise not setup by this
            !! point and will not accept changes like factor levels.
            call PCSetType(pc_inner, PCILU, ierr); CHKERRQ(ierr)
            call PCSetUp(PC_inner, ierr); CHKERRQ(ierr)

            call PCFactorSetLevels(pc_inner, ctx%t2fillin, ierr); CHKERRQ(ierr)

          enddo
          deallocate(sub_ksps)
          nullify(sub_ksps)
        endif

        ctx%firstT2call = PETSC_FALSE
      endif

  end subroutine CPRSetupT2

!  end ofCPR Setup Routines 
!*******************************************************

!*******************************************************
!  CPR Creation Routines  

  subroutine CPRmake(P, ctx, C, ierr)
      use Option_module

      PC :: P
      PetscErrorCode :: ierr
      type(CPRPC) :: ctx
      MPI_Comm C

      call PCSetType(p,PCSHELL,ierr); CHKERRQ(ierr)
      call PCShellSetApply(P,CPRapply,ierr); CHKERRQ(ierr)
      call PCShellSetSetUp(P,CPRSetup,ierr); CHKERRQ(ierr)

      call PCShellSetContext(P, ctx, ierr); CHKERRQ(ierr)

      call CPRCreateT1(C, ctx,  ierr); CHKERRQ(ierr)
      call CPRCreateT2(C, ctx,  ierr); CHKERRQ(ierr)
  end subroutine CPRmake

  SUBROUTINE CPRCreateT1(C,  ctx,   ierr)
      use Option_module

      !! This will perform bare minimum
      !! creation of objects for T1 that do not
      !! need an existing system (i.e. matrix A).
      !! The system dependent setup must be repeated
      !! every step and therefore is seperated out.

      PetscErrorCode :: ierr
      PetscInt b,Aq,Ar,AM,AN,start,end
      KSP solver
      PC prec
      MPI_Comm C
      type(CPRPC) :: ctx
      real :: fin, st


      !! nice default options for boomeramg:
      if (.NOT. ctx%amg_manual) then 
        call SetCPRDefaults(ierr)
      endif

      call KSPCreate(C,solver,ierr); CHKERRQ(ierr)

      select case(ctx%T1_type)
        case('RICHARDSON')
          call KSPSetType(solver,KSPRICHARDSON,ierr); CHKERRQ(ierr)
        case('FGMRES')
          call KSPSetType(solver,KSPFGMRES,ierr); CHKERRQ(ierr)
          call KSPSetTolerances(solver, 1.0d-3, 1.d0-3, PETSC_DEFAULT_REAL, PETSC_DEFAULT_INTEGER, ierr);CHKERRQ(ierr) 
        case('GMRES')
          call KSPSetType(solver,KSPGMRES,ierr); CHKERRQ(ierr)
        case default
          !! really we will just skip over the ksp entirely
          !! in this case, but for completeness..
          call KSPSetType(solver,KSPPREONLY,ierr); CHKERRQ(ierr)
      end select

      call KSPGetPC(solver,prec,ierr); CHKERRQ(ierr)

      if (ctx%useGAMG) then
        call PCSetType(prec,PCGAMG,ierr); CHKERRQ(ierr)
      else
        call PCSetType(prec,PCHYPRE,ierr); CHKERRQ(ierr)
        call PCHYPRESetType(prec,"boomeramg",ierr); CHKERRQ(ierr)
      endif

      call KSPSetFromOptions(solver,ierr); CHKERRQ(ierr)
      call PetscObjectSetName(solver,"T1",ierr); CHKERRQ(ierr)

      ctx%T1solver = solver
      call PCCreate(C,ctx%T1,ierr); CHKERRQ(ierr)
      call PCSetType(ctx%T1,PCSHELL,ierr); CHKERRQ(ierr)
      call PCShellSetApply(ctx%T1,CPRT1apply,ierr); CHKERRQ(ierr)

      call PCShellSetContext(ctx%T1, ctx, ierr); CHKERRQ(ierr)

  END SUBROUTINE CPRCreateT1

  subroutine CPRCreateT2(C, ctx, ierr)
      use Option_module

      PetscErrorCode :: ierr
      MPI_Comm C
      PC T2,P
      Mat A
      KSP solver
      type(CPRPC) :: ctx

      call PCCreate(C,T2,ierr); CHKERRQ(ierr)

      !select case(ctx%T2_type)
      select case(trim(ctx%T2_type))
        case('SAILS')
          call PCSetType(T2,PCHYPRE,ierr); CHKERRQ(ierr)
          call PCHYPRESetType(T2,"parasails",ierr); CHKERRQ(ierr)
        case('PBJ')
          call PCSetType(T2,PCPBJACOBI,ierr); CHKERRQ(ierr)
        case('NONE')
          call PCSetType(T2,PCNONE,ierr); CHKERRQ(ierr)
        case('PCASM')
          call PCSetType(T2,PCASM,ierr); CHKERRQ(ierr)
        case('PCGASM')
          call PCSetType(T2,PCGASM,ierr); CHKERRQ(ierr)
        case('PILUT')
          call PCSetType(T2,PCHYPRE,ierr); CHKERRQ(ierr)
          call PCHYPRESetType(T2,"pilut",ierr); CHKERRQ(ierr)
        case('EUCLID')
          call PCSetType(T2,PCHYPRE,ierr); CHKERRQ(ierr)
          call PCHYPRESetType(T2,"euclid",ierr); CHKERRQ(ierr)
        case('ILU')
          call PCSetType(T2,PCILU,ierr); CHKERRQ(ierr)
        case default
          call PCSetType(T2,PCBJACOBI,ierr); CHKERRQ(ierr)
      end select

      ctx%T2 = T2

  end subroutine CPRCreateT2

!  end of CPR Creation Routines  
!*******************************************************

!*******************************************************
! suplementary setup/init/deinit/routines  

  subroutine CPRStore_Initialize(ctx)
  !! MUST CALL THIS before doing anything with the module
    implicit none
    type(CPRPC) :: ctx

    !! ensure that first run flags are set correctly
    ctx%firstT1Call = PETSC_TRUE
    ctx%firstT2Call = PETSC_TRUE

    ctx%T1_type = "NONE"
    ctx%T2_type = "Jacobi"
    ctx%extract_type = "QIMPES_VARIABLE"

    ctx%asmfactorinplace = PETSC_FALSE
    ctx%t2shiftinblocks = PETSC_FALSE
    ctx%mv_output= PETSC_FALSE
    ctx%t2_zeroing= PETSC_FALSE
    ctx%skip_T1= PETSC_FALSE
    ctx%amg_report = PETSC_FALSE
    ctx%amg_manual = PETSC_FALSE

    ctx%zeroing = PETSC_FALSE
    ctx%useGAMG= PETSC_FALSE


    ctx%t2fillin = 0
    ctx%timescalled = 0

    ctx%asmoverlap = 0

    ctx%exrow_offset = 0

  end subroutine CPRStore_Initialize

  subroutine SetCPRDefaults(ierr)
    use Option_module
    implicit none
    PetscErrorCode :: ierr
    character(len=MAXSTRINGLENGTH) :: string, word


    !! set sensible defualts for the boomeramg solver here,
    !! the defaults it comes with are rarely preferable to
    !! us.

    !! strong threshold, 0.5 is ok for 3D
    string =  '-pc_hypre_boomeramg_strong_threshold'
    word   =  '0.5'
    call PetscOptionsSetValue(PETSC_NULL_OPTIONS, &
                              trim(string),trim(word), &
                              ierr);CHKERRQ(ierr)

    !! coarsen type, PMIS is generally more efficient 
    string =  '-pc_hypre_boomeramg_coarsen_type'
    word   =  'PMIS'
    call PetscOptionsSetValue(PETSC_NULL_OPTIONS, &
                              trim(string),trim(word), &
                              ierr);CHKERRQ(ierr)

    !! interpolation type, ext+i is reccomended 
    string =  '-pc_hypre_boomeramg_interp_type'
    word   =  'ext+i'
    call PetscOptionsSetValue(PETSC_NULL_OPTIONS, &
                              trim(string),trim(word), &
                              ierr);CHKERRQ(ierr)

    !! relaxer, Jacobi should be super weak but cheap 
    string =  '-pc_hypre_boomeramg_relax_type_all'
    word   =  'Jacobi'
    call PetscOptionsSetValue(PETSC_NULL_OPTIONS, &
                              trim(string),trim(word), &
                              ierr);CHKERRQ(ierr)

  end subroutine SetCPRDefaults

  subroutine AllocateWorkersInCPRStash(ctx, n, b)

      type(CPRPC) :: ctx
      PetscInt :: n, b
      PetscInt :: entriesInARow, entriesInAReducedRow

      !! n: the maxiumum number of nonzero columns in any row
      !!    of the matrix we will work with
      !! b: the block size of the matrix we will work with

      !! entriesInARow: the maximum number of entries in a row in
      !!               the matrix we will work with, plus a
      !!               safety buffer in case it was wrong
      entriesInARow = n + 10

      !! entriesInAReducedRow: the maximum number of entries in a row
      !!                       of the extracted matrix we will work with.
      !!
      entriesInAReducedRow = n/b
      !!                       plus a safety buffer in case it was wrong
      entriesInAReducedRow = entriesInAReducedRow + 10

      !! vals: a real buffer to hold the matrix values output from
      !!       matgetrows
      allocate(ctx%vals (0:entriesInARow))
      !! insert_vals: a real buffer to hold the values we will
      !!               insert to the reduced matrix
      allocate(ctx%insert_vals (0:entriesInAReducedRow))
      !! colIdx: an integer buffer to hold the column indexes
      !!         output from matgetvals
      allocate(ctx%colIdx (0:entriesInARow))
      !! colIdx_keep: will copy colIdx into here, since matrestorerows
      !!              resets colIdx
      allocate(ctx%colIdx_keep (0:entriesInARow))
      !! insert_colIdx: an integer buffer of the column indexes
      !!                to be input to the reduced matrix we will
      !!                work with
      allocate(ctx%insert_colIdx (0:entriesInAReducedRow))
      !! all_vals: will just copy every value from the current row block
      !!           (i.e. set of b rows) into here to work on more
      !!           easilly. Can very much be improved.
      allocate(ctx%all_vals (0:b-1, 0:entriesInARow))

  end subroutine AllocateWorkersInCPRStash

  subroutine DeallocateWorkersInCPRStash(ctx)

      type(CPRPC) :: ctx

      deallocate(ctx%vals)
      deallocate(ctx%insert_vals)
      deallocate(ctx%colIdx)
      deallocate(ctx%colIdx_keep)
      deallocate(ctx%insert_colIdx)
      deallocate(ctx%all_vals)

  end subroutine DeallocateWorkersInCPRStash

! end of suplementary setup/init/deinit/routines  
!*******************************************************



!!****************************************************
  !!  FIMPES is very very rough, and should really not be used except for interest:
    SUBROUTINE MatGetSubFIMPES(A, Ap, ierr)

      implicit none
      Mat :: A, Ap
      PetscInt :: i, j,k, numcols
      PetscInt, dimension(:), allocatable :: row_dexs
      !! want to get values from every other column. In reality
      !! every column will be

      PetscErrorCode :: ierr


      PetscInt :: b, rws, cls, nblks, nblks_l, firstRow, cur_coldex, ncolblks

      !! presumably max nonzero pattern size is
      !! (6x3) + 3 = 21
      !! so try harcoding, say, 30 and see if there's
      !! problems?
      !! Then need vals to be of size 90 since
      !! assume doing 3 rows at a time

      !! arrays of variable length:
      PetscReal, dimension(0:90):: vals
      !PetscReal, dimension(:), allocatable :: vals
      PetscReal, dimension(0:30) :: insert_vals
      !PetscReal, dimension(:), allocatable :: insert_vals
      PetscInt, dimension(0:30) :: colIdx
      !PetscInt, dimension(:), allocatable :: colIdx
      PetscInt, dimension(0:30) :: insert_colIdx
      !PetscInt, dimension(:), allocatable :: insert_colIdx

      !! arrays of fixed length:
      PetscInt, dimension(0:0) :: insert_rows

     PetscMPIInt :: rnk, r_st, r_nd
     PetscInt :: ierror


      call MPI_Comm_Rank(PETSC_COMM_WORLD, rnk, ierror)
      call MatGetOwnershipRange(A, r_st, r_nd, ierr); CHKERRQ(ierr)

      !! get number of row blocks
      !! by 'row block' I mean groups of rows that correspond to
      !! the block size
      !! i.e., if block size is 3, then every 3 blocks starts a
      !! new row block.
      !! Given the matrix comes from a FV discretization, then the
      !! pattern of popluated columns should be the same in each row block,
      !! allowing us to cut down on the number of matgetrow calls needed to
      !! determine which columns are populated
      call MatGetBlockSize(A,b,ierr); CHKERRQ(ierr)
      call MatGetSize(A, rws, cls, ierr); CHKERRQ(ierr)
      if (rws .ne. cls) then
        print *, "WARNING: setting up T1 on a nonsquare matrix"
      endif
      allocate(row_dexs (0:b-1))
      nblks = rws/b
      nblks_l = (r_nd-r_st)/b

      !! loop over the row blocks

      do i = 0,nblks_l-1

        firstRow = i*b + r_st

        do j = 0,b-1
          row_dexs(j) = firstRow + j
        end do


        call MatGetRow(A, firstRow, numcols, colIdx, PETSC_NULL_SCALAR, ierr)
        CHKERRQ(ierr)

        if (numcols > 30) then
          print *, "ERROR:"
          print *, "30 as max num nonzeros guess was wrong"
          print *, "numcols: ", numcols
          print *, "(implementation problem, tell Daniel, sorry)"
        endif

        call MatGetValues(A, b, row_dexs, numcols, colIdx(0:numcols-1),  vals, &
                          ierr); CHKERRQ(ierr)


        !! we want to take the first column of every block
        ncolblks = numcols/b


        do j = 0,ncolblks-1

          cur_coldex = j*b
          insert_colIdx(j) = colIdx(cur_coldex)/b
          !! can accumulate here by looping over vals.
          !! There may be a more efficient dynamic programming
          !! way to do this, but hopefully the fact that I'm
          !! not doing full loops helps
          insert_vals(j) = 0d0
          !! idea is as folows:
          !! 1) first entry in vals corresponding to this col
          !!    is at j*b
          !! 2) then the next entry is at j*b + numcols, and etc
          do k = 0,b-1
            insert_vals(j) = insert_vals(j) + vals(cur_coldex + k*numcols)
                                 !! could keep counter instead of k*ncols
                                 !! for slight efficiency increase
          end do
        end do

        insert_rows(0) = i + r_st/b

        call MatSetValues(Ap, 1, insert_rows, ncolblks, &
                          insert_colIdx(0:ncolblks-1), &
                          insert_vals(0:ncolblks-1), INSERT_VALUES, ierr); CHKERRQ(ierr)


        call MatRestoreRow(A, firstRow, numcols, colIdx, PETSC_NULL_SCALAR, &
                           ierr); CHKERRQ(ierr)

      end do

      deallocate(row_dexs)

        call MatAssemblyBegin(Ap,MAT_FINAL_ASSEMBLY,ierr); CHKERRQ(ierr)
        call MatAssemblyEnd(Ap,MAT_FINAL_ASSEMBLY,ierr); CHKERRQ(ierr)

    END SUBROUTINE MatGetSubFIMPES

   SUBROUTINE MatGetSubQIMPES(A, Ap, factors1Vec, factors2Vec, ierr, ctx)

      implicit none
      Mat :: A, Ap
      Vec :: factors1Vec, factors2Vec
      PetscInt :: N
      PetscInt ::  start_row, end_row, i, j,k, numcols, numcols_keep
      PetscInt, dimension(1) :: row_dex
      PetscErrorCode :: ierr
      PetscInt :: b, rws, cls, nblks, nblks_l, firstRow, cur_coldex, ncolblks
      PetscBool :: doZeroing

      PetscInt, dimension(0:0) :: insert_rows

      PetscReal :: aa,bb,cc,dd,ee,ff,gg,hh,ii
      PetscInt :: firstrowdex, loopdex
      PetscReal :: det, fac0, fac1, fac2, sm
      PetscMPIInt :: rnk, r_st, r_nd

      type(CPRPC) :: ctx

      ctx%vals = 0.d0
      ctx%insert_vals = 0.d0
      ctx%all_vals = 0.d0

      ctx%colIdx = 0
      ctx%colIdx_keep = 0
      ctx%insert_colIdx = 0

      call MPI_Comm_Rank(PETSC_COMM_WORLD, rnk, ierr)
      call MatGetOwnershipRange(A, r_st, r_nd, ierr); CHKERRQ(ierr)
      call MatGetBlockSize(A,b,ierr); CHKERRQ(ierr)
      call MatGetSize(A, rws, cls, ierr); CHKERRQ(ierr)
      if (rws .ne. cls) then
        print *, "WARNING: setting up T1 on a nonsquare matrix"
      endif
      nblks = rws/b
      nblks_l = (r_nd-r_st)/b

      sm = 0.d0

      !! loop over the row blocks
      do i = 0,nblks_l-1

        firstRow = i*b + r_st

        !! a) extract first row
        call MatGetRow(A, firstRow, numcols, ctx%colIdx, ctx%vals, ierr); CHKERRQ(ierr)
        !! store vals since we have to put them back
        !! store colIdx this time as well
        do j = 0,numcols-1
          ctx%all_vals(1, j) = ctx%vals(j)
          ctx%colIdx_keep(j) = ctx%colIdx(j)
        end do
        !! b) we can get index of diagonal block here
        firstrowdex = -1
        do loopdex = 0,numcols-1,3
          if (ctx%colIdx(loopdex) .EQ. firstrow) then
            firstrowdex = loopdex
          endif
        enddo
        if (firstrow .EQ. -1) then
          print *, "problem"
        endif
        aa = ctx%vals(firstrowdex)
        bb = ctx%vals(firstrowdex+1)
        cc = ctx%vals(firstrowdex+2)
        !! restore
        call MatRestoreRow(A, firstRow, numcols, ctx%colIdx, ctx%vals, ierr)
        CHKERRQ(ierr)

        !! c) second row
        call MatGetRow(A, firstRow+1, numcols, PETSC_NULL_INTEGER, ctx%vals, &
                      ierr); CHKERRQ(ierr)
        do j = 0,numcols-1
          ctx%all_vals(2, j) = ctx%vals(j)
        end do
        dd = ctx%vals(firstrowdex)
        ee = ctx%vals(firstrowdex+1)
        ff = ctx%vals(firstrowdex+2)
        call MatRestoreRow(A, firstRow+1, numcols, PETSC_NULL_INTEGER, &
                           ctx%vals, ierr); CHKERRQ(ierr)

        !! d) third row
        call MatGetRow(A, firstRow+2, numcols, PETSC_NULL_INTEGER, ctx%vals, &
                     ierr); CHKERRQ(ierr)
        do j = 0,numcols-1
          ctx%all_vals(3, j) =ctx%vals(j)
        end do
        gg = ctx%vals(firstrowdex)
        hh = ctx%vals(firstrowdex+1)
        ii = ctx%vals(firstrowdex+2)
        numcols_keep = numcols
        call MatRestoreRow(A, firstRow+2, numcols, PETSC_NULL_INTEGER, &
                           ctx%vals, ierr); CHKERRQ(ierr)

        !! e) factors
        sm = abs(aa)+abs(dd)+abs(gg)
        det = aa*(ee*ii - ff*hh) - bb*(dd*ii-ff*gg) + cc*(dd*hh-ee*gg)
        fac0 = sm*(ee*ii-ff*hh)/det
        fac1 = sm*(cc*hh-bb*ii)/det
        fac2 = sm*(bb*ff-cc*ee)/det

        !! f) store vectors
        call VecSetValue(factors1Vec, firstRow, fac0, INSERT_VALUES, ierr)
        CHKERRQ(ierr)
        call VecSetValue(factors1Vec, firstRow+1, fac1, INSERT_VALUES, ierr)
        CHKERRQ(ierr)
        call VecSetValue(factors1Vec, firstRow+2, fac2, INSERT_VALUES, ierr)
        CHKERRQ(ierr)

        !! g) prepare to set values
        insert_rows(0) = i + r_st/b
        ncolblks = numcols_keep/b
        do j = 0,ncolblks-1
          cur_coldex = j*b
          ctx%insert_colIdx(j) = ctx%colIdx_keep(cur_coldex)/b


          ctx%insert_vals(j) = fac0*ctx%all_vals(1, cur_coldex)
          ctx%insert_vals(j) = ctx%insert_vals(j) + fac1*ctx%all_vals(2,cur_coldex)
          ctx%insert_vals(j) = ctx%insert_vals(j) + fac2*ctx%all_vals(3,cur_coldex)
        end do

        !! h) set values
        call MatSetValues(Ap, 1, insert_rows, ncolblks, &
                          ctx%insert_colIdx(0:ncolblks-1), &
                          ctx%insert_vals(0:ncolblks-1), INSERT_VALUES, ierr)
        CHKERRQ(ierr)


      end do
      call MatAssemblyBegin(Ap,MAT_FINAL_ASSEMBLY,ierr); CHKERRQ(ierr)
      call MatAssemblyEnd(Ap,MAT_FINAL_ASSEMBLY,ierr); CHKERRQ(ierr)

      call VecAssemblyBegin(factors1Vec, ierr);CHKERRQ(ierr)
      call VecAssemblyEnd(factors1vec, ierr);CHKERRQ(ierr)

      call VecAssemblyBegin(factors2Vec, ierr);CHKERRQ(ierr)
      call VecAssemblyEnd(factors2vec, ierr);CHKERRQ(ierr)

   END SUBROUTINE MatGetSubQIMPES
!!****************************************************
   SUBROUTINE MatGetSubQIMPES_var(A, Ap, factors1Vec, factors2Vec, ierr,N, &
                               doZeroing, b, do_debug_out, ctx)

      implicit none
      Mat :: a, ap
      Vec :: factors1vec, factors2vec
      PetscInt :: N
      PetscInt ::  start_row, end_row, i, j,k, numcols, numcols_keep
      PetscInt, dimension(1) :: row_dex
      PetscErrorCode :: ierr
      PetscInt :: b, rws, cls, nblks, nblks_l, firstRow, cur_coldex, ncolblks
      PetscBool :: doZeroing, do_debug_out

      PetscInt, dimension(0:0) :: insert_rows
      PetscReal, dimension(0:b-1,0:b-1) :: diag_block, inv_diag_block
      PetscReal, dimension(0:b-1) :: local_factors

      PetscInt :: firstrowdex, loopdex
      PetscReal :: det, fac0, fac1, fac2, sm
      PetscMPIInt :: rnk, r_st, r_nd

      PetscReal :: offdiagsum, diagpart, sc, ddf

      type(CPRPC) :: ctx

      integer, dimension(1:b) :: ipiv
      PetscReal, dimension(1:b) :: work
      integer :: lwork, invinfo, luinfo
      lwork = b

      ctx%vals = 0.d0
      ctx%insert_vals = 0.d0
      ctx%all_vals = 0.d0

      ctx%colIdx = 0
      ctx%colIdx_keep = 0
      ctx%insert_colIdx = 0

      call MPI_Comm_Rank(PETSC_COMM_WORLD, rnk, ierr)
      call MatGetOwnershipRange(A, r_st, r_nd, ierr); CHKERRQ(ierr)
      call MatGetSize(A, rws, cls, ierr); CHKERRQ(ierr)
      nblks = rws/b
      nblks_l = (r_nd-r_st)/b

      !! loop over the row blocks
      do i = 0,nblks_l-1

        firstRow = i*b + r_st

        !! first row special treatment
        call MatGetRow(A, firstRow, numcols, ctx%colIdx, ctx%vals, ierr); CHKERRQ(ierr)

        !print *, "ncols here: ", numcols 

        !! store both values of row and the col indexs
        do k = 0,numcols-1
          ctx%all_vals(0, k) = ctx% vals(k)
          ctx%colIdx_keep(k) = ctx%colIdx(k)
        end do
        !! get index of diagonal block
        firstrowdex = -1
        do loopdex = 0,numcols-1,b
          if (ctx%colIdx(loopdex) .EQ. firstrow) then
            firstrowdex = loopdex
          endif
        enddo
        if (firstrow .EQ. -1) then
          print *, "problem"
        endif
        numcols_keep = numcols
        !! restore first row
        call MatRestoreRow(A, firstRow, numcols, ctx%colIdx, ctx%vals, ierr)
        CHKERRQ(ierr)

        !! loop over remaining rows
        do j = 1,b-1
          call MatGetRow(A, firstRow+j, numcols, PETSC_NULL_INTEGER, ctx%vals, &
                         ierr); CHKERRQ(ierr)
          !! harvest values
          do k = 0,numcols-1
            ctx%all_vals(j, k) = ctx%vals(k)
          end do
          call MatRestoreRow(A, firstRow+j, numcols, PETSC_NULL_INTEGER, &
                             ctx%vals, ierr); CHKERRQ(ierr)
        enddo

        !! get inverse of block
        diag_block = ctx%all_vals(0:b-1, firstrowdex:firstrowdex+b-1)
        !! invert
        !! NOTE: this can fail and seemingly just return permutation
        call DGETRF(b, b, diag_block, b, IPIV, luinfo ) !! factorize first
        call DGETRI(b,   diag_block,       b,   IPIV,   WORK,     LWORK, invinfo)
        !call DGETLI(b,   diag_block,       b,   IPIV,   WORK,     LWORK, invinfo)

        if (invinfo > 0) then
          print *, "ERROR: a block in the Jacobian diagonal was &
                    singular"
        endif
        !! scaling factor: the sum of abs of the first column of
        !! diagonal
        sm = 0.d0
        do j = 0,b-1
          !sm = sm + abs(ctx%all_vals(j, firstrowdex))
          sm = sm + abs(ctx%all_vals(j, firstrowdex+ctx%exrow_offset))
        end do
        !! factors: take the top row of the inverse block
        !! and scale
        do j = 0,b-1
            local_factors(j) =  sm*diag_block(0, j) !! diag block has been
                                                    !! replaced with inverse by this point

           !call VecSetValue(factors1Vec, firstRow+j, local_factors(j), &
                             !INSERT_VALUES, ierr); CHKERRQ(ierr)
        end do

        offdiagsum = 0.d0
        diagpart   = 0.d0

        !! prepare to set values
        insert_rows(0) = i + r_st/b
        ncolblks = numcols_keep/b

        do j = 0,ncolblks-1
          cur_coldex = j*b
          ctx%insert_colIdx(j) = ctx%colIdx_keep(cur_coldex)/b

          ctx%insert_vals(j) = 0.d0
          do k = 0,b-1
            ctx%insert_vals(j) = ctx%insert_vals(j) + local_factors(k)*ctx%all_vals(k, cur_coldex)
          enddo

          if (ctx%insert_colIdx(j) .EQ. insert_rows(0)) then
            diagpart = abs(ctx%insert_vals(j))
          else
            offdiagsum = offdiagsum + abs(ctx%insert_vals(j))
          endif

        end do


        do j = 0,b-1
           call VecSetValue(factors1Vec, firstRow+j, local_factors(j), &
                             INSERT_VALUES, ierr); CHKERRQ(ierr)
        end do


        !! set values
        call MatSetValues(Ap, 1, insert_rows, ncolblks, &
                          ctx%insert_colIdx(0:ncolblks-1), &
                          ctx%insert_vals(0:ncolblks-1), INSERT_VALUES, ierr)
         CHKERRQ(ierr)


      end do
      call MatAssemblyBegin(Ap,MAT_FINAL_ASSEMBLY,ierr); CHKERRQ(ierr)
      call MatAssemblyEnd(Ap,MAT_FINAL_ASSEMBLY,ierr); CHKERRQ(ierr)

      call VecAssemblyBegin(factors1Vec, ierr);CHKERRQ(ierr)
      call VecAssemblyEnd(factors1vec, ierr);CHKERRQ(ierr)

   END SUBROUTINE MatGetSubQIMPES_var

!!****************************************************
   SUBROUTINE CalcDDF(ctx, ncolblks, b, local_factors, cur_row, ddf)
      implicit none
      PetscReal :: offdiagsum, diagpart
      PetscInt :: j, k,  ncolblks, cur_coldex, b, cur_row, coldex
      type(CPRPC) :: ctx
      PetscReal, dimension(0:b-1) :: local_factors
      PetscReal :: ddf

        offdiagsum = 0.d0
        diagpart   = 0.d0


        !! prepare to set values
        do j = 0,ncolblks-1
          cur_coldex = j*b

          coldex = ctx%colIdx_keep(cur_coldex)/b

          ctx%insert_vals(j) = 0.d0
          do k = 0,b-1
            ctx%insert_vals(j) = ctx%insert_vals(j) + local_factors(k)*ctx%all_vals(k, cur_coldex)
          enddo

          if (coldex  .EQ. cur_row) then
            diagpart = abs(ctx%insert_vals(j))
          else
            offdiagsum = offdiagsum + abs(ctx%insert_vals(j))
          endif

        end do


        ddf = diagpart - offdiagsum

        !! make sure to zero out insert_vals:
        ctx%insert_vals = 0.d0

   END SUBROUTINE CalcDDF


   SUBROUTINE MatGetMaxRowCount(A, mx, ierr)

      implicit none
      Mat :: A
      PetscInt :: mx, mx_loc, r_st, r_nd, i, numcols
      PetscErrorCode :: ierr

      call MatGetOwnershipRange(A, r_st, r_nd, ierr); CHKERRQ(ierr)

      mx = 0
      mx_loc = 0

      do i = r_st,r_nd-1
        call MatGetRow(A, i, numcols, PETSC_NULL_INTEGER, PETSC_NULL_SCALAR, &
                       ierr); CHKERRQ(ierr)
        if (numcols > mx_loc) then
          mx_loc = numcols
        endif
        call MatRestoreRow(A, i, numcols, PETSC_NULL_INTEGER, &
                           PETSC_NULL_SCALAR, ierr); CHKERRQ(ierr)
      end do

      call MPI_Allreduce(mx_loc, mx, ONE_INTEGER_MPI, MPI_INTEGER, MPI_MAX, PETSC_COMM_WORLD, ierr)


   END SUBROUTINE MatGetMaxRowCount

   SUBROUTINE QIRHS(factors, worker, r, rhat, ierr)
     implicit none
     Vec :: factors, worker, r, rhat
     PetscMPIInt :: rnk, r_st, r_nd
     PetscInt :: b, k
     PetscErrorCode :: ierr

     call VecPointwiseMult(worker, factors, r, ierr);CHKERRQ(ierr)
     call VecGetBlockSize(worker,b,ierr); CHKERRQ(ierr)
     k = 0
     call VecStrideGather(worker, k, rhat, INSERT_VALUES, ierr);CHKERRQ(ierr)
     do k = 1,b-1
       call VecStrideGather(worker, k, rhat, ADD_VALUES, ierr);CHKERRQ(ierr)
     end do

   END SUBROUTINE QIRHS

!****************************************************

end module CPR_linsolver
